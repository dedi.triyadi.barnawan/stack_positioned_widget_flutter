import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MyApp());
}

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Application-05"),
//         ),
//         body: Stack(
//           children: [
//             Positioned(
//               child: Container(
//                 margin: EdgeInsets.all(12),
//                 width: 500,
//                 height: 200,
//                 decoration: BoxDecoration(
//                     color: Colors.redAccent,
//                     border: Border.all(
//                       color: Colors.black,
//                       width: 5,
//                     ),
//                     borderRadius: BorderRadius.circular(12)),
//                 child: Stack(
//                   children: [
//                     Positioned(
//                       bottom: 20,
//                       right: 195,
//                       child: Text(
//                         "Dedi",
//                         style: GoogleFonts.syncopate(fontSize: 50),
//                       ),
//                     ),
//                     Positioned(
//                         bottom: 20,
//                         right: 20,
//                         child: Text(
//                           "Triyadi",
//                           style: GoogleFonts.comfortaa(fontSize: 50),
//                         ))
//                   ],
//                 ),
//               ),
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//         home: Scaffold(
//       appBar: AppBar(
//         title: Text("Geek-01"),
//         backgroundColor: Colors.greenAccent[400],
//         centerTitle: true,
//       ),
//       body: Padding(
//         padding: EdgeInsets.only(top: 300),
//         child: Stack(
//           alignment: AlignmentDirectional.center,
//           children: [
//             Positioned(
//               top: 0.0,
//               child: Icon(Icons.message,
//                   size: 128.0, color: Colors.greenAccent[400]),
//             ),
//             Positioned(
//               top: 0.0,
//               right: 285,
//               child: CircleAvatar(
//                 radius: 16,
//                 backgroundColor: Colors.red,
//                 foregroundColor: Colors.white,
//                 child: Text('24'),
//               ),
//             )
//           ],
//         ),
//       ),
//     ));
//   }
// }

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: HomePage(),
//     );
//   }
// }

// class HomePage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Center(
//           child: Container(
//         width: 400,
//         height: 700,
//         // color: Colors.yellow,
//         decoration: BoxDecoration(
//           color: Colors.yellow,
//           border: Border.all(
//             color: Colors.black,
//             width: 5,
//           ),
//         ),
//         child: Stack(
//           alignment: Alignment.topRight,
//           clipBehavior: Clip.none,
//           children: [
//             Positioned(
//               top: 10,
//               left: 10,
//               child: ElevatedButton(
//                 style: ElevatedButton.styleFrom(
//                   primary: Colors.teal,
//                   onPrimary: Colors.black,
//                 ),
//                 onPressed: () {},
//                 child: Text("Submit"),
//               ),
//             ),
//             Positioned(
//               right: 10,
//               top: 10,
//               child: Container(
//                 width: 200,
//                 height: 200,
//                 color: Colors.red,
//               ),
//             ),
//             Positioned(
//               bottom: -20,
//               right: -20,
//               child: Container(
//                 width: 100,
//                 height: 100,
//                 // color: Colors.green,
//                 decoration: BoxDecoration(
//                   color: Colors.green,
//                   border: Border.all(
//                     color: Colors.black,
//                     width: 5,
//                   ),
//                   borderRadius: BorderRadius.circular(50),
//                 ),
//               ),
//             ),
//           ],
//         ),
//       )),
//     );
//   }
// }

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        child: Container(
          child: Row(
            children: [
              Container(
                width: size.width / 2,
                height: size.height / 2,
                color: Colors.red,
              ),
              Container(
                  width: size.width / 2,
                  height: size.height / 2,
                  color: Colors.green)
            ],
          ),
        ),
      ),
    );
  }
}
